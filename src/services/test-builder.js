/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Test builder module
 */
angular.module('seen-test')

/**
 * @ngdoc Services
 * @name $test
 * @description Run predefined test on modules
 * 
 * Inject the service in test and use in the test body.
 * 
 * @example <caption>Test collection method in a service</caption>
 *          $test.serviceCollectionFunction('Book', $books);
 * 
 * This example check all methods of the service related to the Book such as
 * getBook, putBook and etc.
 */
.service('$test', function($rootScope, $httpBackend, $injector) {

	'use strict';
	/**
	 * Checks required function of a model in service
	 * 
	 * @example <caption>Test collection method in a service</caption>
	 *          $test.serviceCollectionFunction('Book', $books);
	 * 
	 * This example check all methods of the service related to the Book such as
	 * getBook, putBook and etc.
	 * 
	 * @param type
	 *            {string} type of the model to test
	 * @param service
	 *            {object} a service to check
	 */
	this.serviceCollectionFunction = function(service, type) {
		expect(type.name).not.toBeNull();
		var pname = seen.pluralName(type.name);
		var sname = seen.singularName(type.name);
		// list
		expect(angular.isFunction(service['get' + pname])).toBe(true);
		expect(angular.isFunction(service['delete' + pname])).toBe(true);
		expect(angular.isFunction(service['put' + pname])).toBe(true);
		// item
		expect(angular.isFunction(service['get' + sname])).toBe(true);
		expect(angular.isFunction(service['put' + sname])).toBe(true);
		// schema
		// expect(angular.isFunction(service[type.toLocaleLowerCase()+'Schema'].commentSchema)).toBe(true);
	};

	this.serviceGetList = function(service, type, done) {
		var pname = seen.pluralName(type.name);
		service['get' + pname]()//
		.then(function(list) {
			expect(list).not.toBeNull();
			expect(list.items).not.toBeUndefined();
			expect(list.current_page).not.toBeUndefined();
			expect(list.page_number).not.toBeUndefined();
			done();
		});
		var result = {
			current_page : 1,
			page_number : 2,
			items : [],
		};
		for (var i = 0; i < 10; i++) {
			result.items.push({
				id : i,
				attr : 'attr value'
			});
		}
		$httpBackend//
			.expect('GET', type.url)//
			.respond(200, result);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	};

	this.servicePutList = function(service, type, done) {
		var pname = seen.pluralName(type.name);
		var sample=[{
			id: 1,
			attr: 'test attr'
		},{
			attr: 'test attr'
		}];
		service['put' + pname](sample)//
		.then(function(list) {
			expect(list).not.toBeNull();
			done();
		});
		var result = {
				current_page : 1,
				page_number : 2,
				items : sample,
		};
		$httpBackend//
			.expect('POST', type.url)//
			.respond(200, result);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	};
	
	this.serviceDeleteList = function(service, type, done) {
		var pname = seen.pluralName(type.name);
		service['delete' + pname]()//
		.then(function(list) {
			expect(list).not.toBeNull();
			done();
		});
		var result = {
			current_page : 1,
			page_number : 2,
			items : [],
		};
		$httpBackend//
			.expect('DELETE', type.url)//
			.respond(200, result);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	};

	this.serviceGetItem = function(service, type, done) {
		var sname = seen.singularName(type.name);
		service['get' + sname](1)//
		.then(function(item) {
			expect(item).not.toBeNull();
			done();
		});
		var result = {
			id : 1,
		};
		$httpBackend//
			.expect('GET', type.url + '/1')//
			.respond(200, result);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	};

	this.servicePutItem = function(service, type, done) {
		var sname = seen.singularName(type.name);
		var sample = {
			attr : 'test attribute'
		};
		service['put' + sname](sample)//
		.then(function(newSample) {
			expect(newSample).not.toBeNull();
			done();
		});
		$httpBackend//
			.expect('POST', type.url)//
			.respond(200, sample);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	};

	
	this.factoryBasicFunctions = function(type) {
		// create factory
		var Factory = $injector.get(type.factory || type.name);
		var factory = new Factory({
			id: 1
		});
		// check functions
		expect(angular.isFunction(factory.update)).toBe(true);
		expect(angular.isFunction(factory.delete)).toBe(true);
	};
	
	this.factoryUpdateFunctions = function(type, done) {
		var sample = {
				id: 1
		};
		// create factory
		var Factory = $injector.get(type.factory || type.name);
		var factory = new Factory(sample);
		
		factory.update()//
		.then(function(newFactory){
			expect(newFactory).not.toBeNull();
			done();
		});
		$httpBackend//
			.expect('POST', type.url + '/1')//
			.respond(200, sample);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	};
	
	this.factoryDeleteFunctions = function(type, done) {
		var sample = {
				id: 1
		};
		// create factory
		var Factory = $injector.get(type.factory || type.name);
		var factory = new Factory(sample);
		
		factory.delete()//
		.then(function(newFactory){
			expect(newFactory).not.toBeNull();
			done();
		});
		$httpBackend//
			.expect('DELETE', type.url + '/1')//
			.respond(200, sample);
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	};
	
	this.factoryCollectionFunctions = function(factory, resource){
		var sample = {
				id: 1,
				type: factory.factory
		};
		
		// create factory
		var Factory = $injector.get(factory.factory || factory.name);
		var factoryItem = new Factory(sample);
		
		var type = _.merge(resource, {
			url: factory.url + '/1' + resource.url
		});
		
		expect(type.name).not.toBeNull();
		var pname = seen.pluralName(type.name);
		// list
		expect(angular.isFunction(factoryItem['get' + pname])).toBe(true);
		expect(angular.isFunction(factoryItem['delete' + pname])).toBe(true);
		expect(angular.isFunction(factoryItem['put' + pname])).toBe(true);
	};
});
