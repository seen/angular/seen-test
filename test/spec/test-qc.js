/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('sort', function () {


	beforeEach(function() {
		module('seen-test');
	});


	it('should build test for collection', function() {
		expect(angular.isDefined(seen.qc)).toBe(true);

		expect(angular.isDefined(seen.qc.int)).toBe(true);
	});


	it('#odd returns true for odd numbers', function() {
		expect(function(i) {
			return angular.isDefined(i);
		}).forAll(seen.qc.int);
	});



	function Animal(type, weight) {
		this.type = type;
		this.weight = weight;
	}

	var randomAnimal = qc.constructor(Animal, qc.pick('tiger', 'cow'), qc.natural);
	

	it('Random object test', function() {
		expect(function(animal) {
			return angular.isDefined(animal);
		}).forAll(randomAnimal);
	});
	
});